var events = require('events');
var util = require('util');

var lampMatrixObject = function(boardNumber, messageDecoder) {
    this.boardNumber = boardNumber;
    this.messageDecoder = messageDecoder;

    var thisInstance = this;

    this.handleCanMessage = function(priority, boardSpecific, boardAddress, featureType, featureNum, messageType, isRequest, data) {
        if(boardAddress == boardNumber) {
            thisInstance.emit('unfilteredLampMatrixMessage', priority, boardSpecific, boardAddress, featureType, featureNum, messageType, isRequest, data);
            switch(messageType) {
                case 0:
                    thisInstance.emit('lampStatusMessage', featureNum, isRequest, data);
                    break;
                case 1:
                    thisInstance.emit('lampDefaultsMessage', featureNum, isRequest, data);
                    break;
                case 2:
                    thisInstance.emit('lampTriggeringMessage', featureNum, isRequest, data);
                    break;
                default:
                    break;
            }
        }
    };

    this.requestStatus = function(lampNumber, priority, boardSpecific) {
        if(arguments.length < 3) boardSpecific = 1;
        if(arguments.length < 2) priority = 0;
        var dataOut = new Buffer([]);
        messageDecoder.sendCanFrame(priority, boardSpecific, boardNumber, 2, lampNumber, 0, true, dataOut);
    };

    this.requestDefaults = function(lampNumber, priority, boardSpecific) {
        if(arguments.length < 3) boardSpecific = 1;
        if(arguments.length < 2) priority = 0;
        var dataOut = new Buffer([]);
        messageDecoder.sendCanFrame(priority, boardSpecific, boardNumber, 2, lampNumber, 1, true, dataOut);
    };

    this.requestTriggering = function(lampNumber, priority, boardSpecific) {
        if(arguments.length < 3) boardSpecific = 1;
        if(arguments.length < 2) priority = 0;
        var dataOut = new Buffer([]);
        messageDecoder.sendCanFrame(priority, boardSpecific, boardNumber, 2, lampNumber, 2, true, dataOut);
    };

    this.setStatus = function(lampNumber, lampStatus, lampOnIntensity, lampOnDuration, lampOffIntensity, lampOffDuration, priority, boardSpecific) {
        if(arguments.length < 8) boardSpecific = 1;
        if(arguments.length < 7) priority = 0;
        if(arguments.length >= 6) {
            var dataOut = new Buffer([lampStatus, lampOnIntensity, lampOnDuration, lampOffIntensity, lampOffDuration]);
        } else if(arguments.length == 5) {
            var dataOut = new Buffer([lampStatus, lampOnIntensity, lampOnDuration, lampOffIntensity]);
        } else if(arguments.length == 4) {
            var dataOut = new Buffer([lampStatus, lampOnIntensity, lampOnDuration]);
        } else if(arguments.length == 3) {
            var dataOut = new Buffer([lampStatus, lampOnIntensity]);
        } else if(arguments.length == 2) {
            var dataOut = new Buffer([lampStatus]);
        } else {
            return;
        }

        messageDecoder.sendCanFrame(priority, boardSpecific, boardNumber, 2, lampNumber, 0, false, dataOut);
    }

    this.setDefaults = function(lampNumber, lampOnIntensity, lampOnDuration, lampOffIntensity, lampOffDuration, priority, boardSpecific) {
        if(arguments.length < 7) boardSpecific = 1;
        if(arguments.length < 6) priority = 0;
        if(arguments.length >= 5) {
            var dataOut = new Buffer([lampOnIntensity, lampOnDuration, lampOffIntensity, lampOffDuration]);
        } else if(arguments.length == 4) {
            var dataOut = new Buffer([lampOnIntensity, lampOnDuration, lampOffIntensity]);
        } else if(arguments.length == 3) {
            var dataOut = new Buffer([lampOnIntensity, lampOnDuration]);
        } else if(arguments.length == 2) {
            var dataOut = new Buffer([lampOnIntensity]);
        } else {
            return;
        }

        messageDecoder.sendCanFrame(priority, boardSpecific, boardNumber, 2, lampNumber, 1, false, dataOut);
    }

    this.setTriggering = function(lampNumber, changeFlag, priority, boardSpecific) {
        if(arguments.length < 4) boardSpecific = 1;
        if(arguments.length < 3) priority = 0;
        if(changeFlag) {
            var triggerByte = 1;
        } else {
            var triggerByte = 0;
        }
        var dataOut = new Buffer([triggerByte]);
        messageDecoder.sendCanFrame(priority, boardSpecific, boardNumber, 2, lampNumber, 2, false, dataOut);
    };
}
util.inherits(lampMatrixObject, events.EventEmitter);
exports.createLampMatrixObject = function(boardNum, messageDecoder) { return new lampMatrixObject(boardNum, messageDecoder); };