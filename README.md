# Pinball Software

## Running Unit Tests

### Node.js

Install node.js and npm in your OS of choice, e.g. in Ubuntu:

    sudo apt-get install nodejs npm

If your OS-specific package doesn't provide a `node` executable you may have to
create one yourself. E.g. un Ubuntu:

    sudo ln -s /usr/bin/nodejs /usr/bin/node

This is required for nodeunit since it assumes that node.js is runnable via `node`.

### nodeunit

The unit test framework used is [nodeunit](https://github.com/caolan/nodeunit).
To install it using npm:

    sudo npm install -g nodeunit
    npm install nodeunit

To get the nodeunit executable in your `$PATH` installing with `-g` is
required. For some reason node.js will not find the nodeunit library if you
don't install it without `-g` also.

### Running the Test

To run the unit tests, simply run `nodeunit` in the pinball software folder:

    nodeunit

It will find and execute and test cases on its own. You should see something like:

    % nodeunit

    test_drop_targets
    ✔ IO Classes - Light
    ✔ IO Classes - Solenoid
    ✔ IO Classes - Switch
    ✔ DropTarget

    OK: 14 assertions (161ms)
