var events = require('events');
var util = require('util');

var scoreModuleObject = function(boardNumber, messageDecoder) {
    this.boardNumber = boardNumber;
    this.messageDecoder = messageDecoder;

    var thisInstance = this;

    this.handleCanMessage = function(priority, boardSpecific, boardAddress, featureType, featureNum, messageType, isRequest, data) {
        if(boardAddress == boardNumber) {
            thisInstance.emit('unfilteredScoreModuleMessage', priority, boardSpecific, boardAddress, featureType, featureNum, messageType, isRequest, data);
            switch(messageType) {
                case 0:
                    thisInstance.emit('scoreStatusMessage', featureNum, isRequest, data);
                    var scoreData = new Buffer(data);
                    var scoreValue = scoreData.readInt32BE(0);
                    thisInstance.emit('scorePlayer' + featureNum + 'Status', featureNum, isRequest, scoreValue, data);
                    break;
                case 1:
                    thisInstance.emit('scoreIncrementMessage', featureNum, isRequest, data);
                    var scoreData = new Buffer(data);
                    var scoreValue = scoreData.readInt32BE(0);
                    thisInstance.emit('scorePlayer' + featureNum + 'Increment', featureNum, isRequest, scoreValue, data);
                    break;
                case 2:
                    thisInstance.emit('rawDisplayMessage', featureNum, isRequest, data);
                    thisInstance.emit('displayPlayer' + featureNum + 'Raw', featureNum, isRequest, data);
                    break;
                case 3:
                    thisInstance.emit('brightnessMessage', featureNum, isRequest, data);
                    var scoreData = new Buffer(data);
                    var brightness = scoreData.readUInt8(0);
                    thisInstance.emit('scorePlayer' + featureNum + 'Brightness', featureNum, isRequest, brightness, data);
                    break;
                case 4:
                    thisInstance.emit('scoreTriggeringMessage', featureNum, isRequest, data);
                    break;
                default:
                    break;
            }
        }
    };

    this.requestScore = function(playerNumber, priority, boardSpecific) {
        if(arguments.length < 3) boardSpecific = 1;
        if(arguments.length < 2) priority = 0;
        var dataOut = new Buffer([]);
        messageDecoder.sendCanFrame(priority, boardSpecific, boardNumber, 4, playerNumber, 0, true, dataOut);
    };

    this.setScore = function(playerNumber, newScore, priority, boardSpecific) {
        if(arguments.length < 4) boardSpecific = 1;
        if(arguments.length < 3) priority = 0;
        var dataOut = new Buffer(4);
        dataOut.writeInt32BE(newScore, 0);
        messageDecoder.sendCanFrame(priority, boardSpecific, boardNumber, 4, playerNumber, 0, false, dataOut);
    };

    this.addScore = function(playerNumber, scoreIncrement, priority, boardSpecific) {
        if(arguments.length < 4) boardSpecific = 1;
        if(arguments.length < 3) priority = 0;
        var dataOut = new Buffer(4);
        dataOut.writeInt32BE(scoreIncrement, 0);
        messageDecoder.sendCanFrame(priority, boardSpecific, boardNumber, 4, playerNumber, 1, false, dataOut);
    };

    this.requestDisplay = function(playerNumber, priority, boardSpecific) {
        if(arguments.length < 3) boardSpecific = 1;
        if(arguments.length < 2) priority = 0;
        var dataOut = new Buffer([]);
        messageDecoder.sendCanFrame(priority, boardSpecific, boardNumber, 4, playerNumber, 2, true, dataOut);
    };

    this.requestBrightness = function(playerNumber, priority, boardSpecific) {
        if(arguments.length < 3) boardSpecific = 1;
        if(arguments.length < 2) priority = 0;
        var dataOut = new Buffer([]);
        messageDecoder.sendCanFrame(priority, boardSpecific, boardNumber, 4, playerNumber, 3, true, dataOut);
    };

    this.setBrightness = function(playerNumber, brightness, priority, boardSpecific) {
        if(arguments.length < 4) boardSpecific = 1;
        if(arguments.length < 3) priority = 0;
        var dataOut = new Buffer(1);
        dataOut.writeUInt8(brightness, 0);
        messageDecoder.sendCanFrame(priority, boardSpecific, boardNumber, 4, playerNumber, 3, false, dataOut);
    };

    this.setDisplay = function(playerNumber, displayArray, priority, boardSpecific) {
        if(arguments.length < 4) boardSpecific = 1;
        if(arguments.length < 3) priority = 0;
        messageDecoder.sendCanFrame(priority, boardSpecific, boardNumber, 4, playerNumber, 2, false, displayArray);
    };

    this.requestTriggering = function(switchNumber, priority, boardSpecific) {
        if(arguments.length < 3) boardSpecific = 1;
        if(arguments.length < 2) priority = 0;
        var dataOut = new Buffer([]);
        messageDecoder.sendCanFrame(priority, boardSpecific, boardNumber, 4, switchNumber, 4, true, dataOut);
    };

    this.setTriggering = function(switchNumber, enableTrigger, priority, boardSpecific) {
        if(arguments.length < 5) boardSpecific = 1;
        if(arguments.length < 4) priority = 0;
        var triggerByte = 0;
        if(enableTrigger) triggerByte += 1;
        var dataOut = new Buffer([triggerByte]);
        messageDecoder.sendCanFrame(priority, boardSpecific, boardNumber, 4, switchNumber, 4, false, dataOut);
    };
}
util.inherits(scoreModuleObject, events.EventEmitter);
exports.createScoreModuleObject = function(boardNum, messageDecoder) { return new scoreModuleObject(boardNum, messageDecoder); };
