
function MultiTarget(switches, lights, completeCB) {
	this.targets = 0;
	this.switches = switches;
	this.lights = lights;
	this.completeCB = completeCB;

	var self = this;

	this.hitSwitch = function(number) {
		self.targets ^= (1 << number);
		if (self.targets & (1 << number)) {
			self.setLight(number);
		} else {
			self.clearLight(number);
		}
		if (self.targets == ((1 << self.switches.length) - 1))
		{
			self.reset();
			self.completeCB();
		}
	};

	this.setLight = function(number) {
		self.lights[number].set(1);
	};

	this.clearLight = function(number) {
		self.lights[number].set(0);
	};

	this.reset = function() {
		if (self.targets != 0x00)
		{
			self.targets = 0x00;

			for (var i = 0; i < self.lights.length; i++) {
				self.lights[i].set(0);
			}
		}
	};

	this._setSwitchCallback = function(swNumber) {
		self.switches[swNumber].addCB(function() {
			self.hitSwitch(swNumber);
		});
	};

	for (var i = 0; i < self.switches.length; i++)
	{
		self._setSwitchCallback(i);
	}
}

exports.MultiTarget = MultiTarget;
