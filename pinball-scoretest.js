var pinballMessageDecoder = require('./pinballMessageDecoder');
var switchObjectFactory = require('./switchMatrixObject');
var lampObjectFactory = require('./lampMatrixObject');
var solenoidObjectFactory = require('./solenoidObject');
var systemManagementObjectFactory = require('./systemManagementObject');
var scoreModuleObjectFactory = require('./scoreModuleObject');

var can = require('can');
var channel = can.createRawChannel("can0", true);

pinballMessageDecoder.setCanChannel(channel);
channel.addListener("onMessage", pinballMessageDecoder.incomingCanFrame);

pinballMessageDecoder.addListener("unfilteredMessage", function(priority, boardSpecific, boardAddress, featureType, featureNum, messageType, isRequest, data) {
    if(isRequest) {
        console.log("Request frame received: Priority " + priority + ", address " + boardAddress + ", feature type " + featureType + ", feature number " + featureNum + ", message type " + messageType + ", data " + data.toString('hex'));
    } else {
        console.log("Data frame received: Priority " + priority + ", address " + boardAddress + ", feature type " + featureType + ", feature number " + featureNum + ", message type " + messageType + ", data " + data.toString('hex'));
    }
});

var board1SystemManagement = systemManagementObjectFactory.createSystemManagementObject(1, pinballMessageDecoder);
var board1Switches = switchObjectFactory.createSwitchMatrixObject(1, pinballMessageDecoder);
var board1Lamps = lampObjectFactory.createLampMatrixObject(1, pinballMessageDecoder);
var board1Solenoids = solenoidObjectFactory.createSolenoidObject(1, pinballMessageDecoder);

var player1Score = scoreModuleObjectFactory.createScoreModuleObject(8, pinballMessageDecoder);

pinballMessageDecoder.addListener("systemManagementMessage", board1SystemManagement.handleCanMessage);
pinballMessageDecoder.addListener("switchMatrixMessage", board1Switches.handleCanMessage);
pinballMessageDecoder.addListener("lampMatrixMessage", board1Lamps.handleCanMessage);
pinballMessageDecoder.addListener("solenoidMessage", board1Solenoids.handleCanMessage);

pinballMessageDecoder.addListener("scoreDisplayMessage", player1Score.handleCanMessage);

//board1SystemManagement.addListener("capabilitiesMessage", function() {
//	console.log("Setup board 1");
//	board1Lamps.setDefaults(0, 255, 64, 0, 64);
//	board1Solenoids.setDefaults(1, 100);
//});

board1Switches.addListener("switch1RisingEdgeMessage", function() {
	console.log("Rise11");
	player1Score.setScore(0, 5000);
});

board1Switches.addListener("switch2RisingEdgeMessage", function() {
	console.log("Add");
	player1Score.addScore(0, 1000);
});

board1Switches.addListener("switch4RisingEdgeMessage", function() {
	console.log("Half bright");
	player1Score.setBrightness(0, 127);
});

board1Switches.addListener("switch5RisingEdgeMessage", function() {
	console.log("Full bright");
	player1Score.setBrightness(0, 255);
});

board1Switches.addListener("switch6RisingEdgeMessage", function() {
	console.log("Off");
	player1Score.setBrightness(0, 0);
});

board1Switches.addListener("switch3RisingEdgeMessage", function() {
	console.log("Dec");
	player1Score.addScore(0, -1000);
});

board1Switches.addListener("switch8RisingEdgeMessage", function() {
	console.log("All on");
	var newDisplay = new Buffer([255, 255, 255, 255, 255, 255, 255, 255]);
	player1Score.setDisplay(0, newDisplay);
});

board1Switches.addListener("switch9RisingEdgeMessage", function() {
	console.log("All off");
	var newDisplay = new Buffer([0, 0, 0, 0, 0, 0, 0, 0]);
	player1Score.setDisplay(0, newDisplay);
});

//board1Switches.addListener("switch1RisingEdgeMessage", function() {
//	board1Solenoids.setStatus(0, 1, 100);
//});

//board1Switches.addListener("switch1FallingEdgeMessage", function() {
//	board1Solenoids.setStatus(1, 1);
//});

channel.start();
//board1SystemManagement.requestCapabilities(0, 0);
player1Score.setTriggering(0, 1);
