var pinballMessageDecoder = require('./pinballMessageDecoder');
var switchObjectFactory = require('./switchMatrixObject');
var lampObjectFactory = require('./lampMatrixObject');
var solenoidObjectFactory = require('./solenoidObject');
var systemManagementObjectFactory = require('./systemManagementObject');
var scoreModuleObjectFactory = require('./scoreModuleObject');
var dt = require('./drop-targets');
var mt = require('./multi-target');

var can = require('socketcan');
var channel = can.createRawChannel("can0", true);
var pinball_lifes=0;

var score = 0;

/*var mpd = require('mpd');
var mpdClient = mpd.connect({
  port: 6600,
  host: 'localhost'
});*/

pinballMessageDecoder.setCanChannel(channel);
channel.addListener("onMessage", pinballMessageDecoder.incomingCanFrame);

pinballMessageDecoder.addListener("unfilteredMessage", function(priority, boardSpecific, boardAddress, featureType, featureNum, messageType, isRequest, data) {
    if(isRequest) {
        console.log("Request frame received: Priority " + priority + ", address " + boardAddress + ", feature type " + featureType + ", feature number " + featureNum + ", message type " + messageType + ", data " + data.toString('hex'));
    } else {
        console.log("Data frame received: Priority " + priority + ", address " + boardAddress + ", feature type " + featureType + ", feature number " + featureNum + ", message type " + messageType + ", data " + data.toString('hex'));
    }
});

var board1SystemManagement = systemManagementObjectFactory.createSystemManagementObject(1, pinballMessageDecoder);
var board1Switches = switchObjectFactory.createSwitchMatrixObject(1, pinballMessageDecoder);
var board1Lamps = lampObjectFactory.createLampMatrixObject(1, pinballMessageDecoder);
var board1Solenoids = solenoidObjectFactory.createSolenoidObject(1, pinballMessageDecoder);

var board2SystemManagement = systemManagementObjectFactory.createSystemManagementObject(2, pinballMessageDecoder);
var board2Switches = switchObjectFactory.createSwitchMatrixObject(2, pinballMessageDecoder);
var board2Lamps = lampObjectFactory.createLampMatrixObject(2, pinballMessageDecoder);
var board2Solenoids = solenoidObjectFactory.createSolenoidObject(2, pinballMessageDecoder);

var board3SystemManagement = systemManagementObjectFactory.createSystemManagementObject(3, pinballMessageDecoder);
var board3Switches = switchObjectFactory.createSwitchMatrixObject(3, pinballMessageDecoder);
var board3Lamps = lampObjectFactory.createLampMatrixObject(3, pinballMessageDecoder);
var board3Solenoids = solenoidObjectFactory.createSolenoidObject(3, pinballMessageDecoder);

var player1Score = scoreModuleObjectFactory.createScoreModuleObject(8, pinballMessageDecoder);
var player2Score = scoreModuleObjectFactory.createScoreModuleObject(9, pinballMessageDecoder);
var player3Score = scoreModuleObjectFactory.createScoreModuleObject(10, pinballMessageDecoder);
var player4Score = scoreModuleObjectFactory.createScoreModuleObject(11, pinballMessageDecoder);

pinballMessageDecoder.addListener("systemManagementMessage", board1SystemManagement.handleCanMessage);
pinballMessageDecoder.addListener("switchMatrixMessage", board1Switches.handleCanMessage);
pinballMessageDecoder.addListener("lampMatrixMessage", board1Lamps.handleCanMessage);
pinballMessageDecoder.addListener("solenoidMessage", board1Solenoids.handleCanMessage);

pinballMessageDecoder.addListener("systemManagementMessage", board2SystemManagement.handleCanMessage);
pinballMessageDecoder.addListener("switchMatrixMessage", board2Switches.handleCanMessage);
pinballMessageDecoder.addListener("lampMatrixMessage", board2Lamps.handleCanMessage);
pinballMessageDecoder.addListener("solenoidMessage", board2Solenoids.handleCanMessage);

pinballMessageDecoder.addListener("systemManagementMessage", board3SystemManagement.handleCanMessage);
pinballMessageDecoder.addListener("switchMatrixMessage", board3Switches.handleCanMessage);
pinballMessageDecoder.addListener("lampMatrixMessage", board3Lamps.handleCanMessage);
pinballMessageDecoder.addListener("solenoidMessage", board3Solenoids.handleCanMessage);

//var player = require("sdlmixer");

//Flippers
// Left
board3Switches.addListener("switch12RisingEdgeMessage", function() {
	board1Solenoids.setStatus(6, 1);

});

board3Switches.addListener("switch12FallingEdgeMessage", function() {
	board1Solenoids.setStatus(6, 0);
});

// Right
board3Switches.addListener("switch8RisingEdgeMessage", function() {
	board1Solenoids.setStatus(7, 1);
});

board3Switches.addListener("switch8FallingEdgeMessage", function() {
	board1Solenoids.setStatus(7, 0);
});
//End Flippers


// Left Kicker
board1Switches.addListener("switch10RisingEdgeMessage", function() {
	board1Solenoids.setStatus(0, 1, 10);
});

// Right Kicker
board1Switches.addListener("switch6RisingEdgeMessage", function() {
	board1Solenoids.setStatus(1, 1, 10);
});

// Left spinner
board3Switches.addListener("switch3RisingEdgeMessage", function() {
	score += 10;
	updateScore();
});

// Right spinner
board2Switches.addListener("switch12RisingEdgeMessage", function() {
	score += 10;
	updateScore();
});

// Oil Pit
board3Switches.addListener("switch2RisingEdgeMessage", function() {
	setTimeout(function() {board3Solenoids.setStatus(2, 1, 10);}, 2000);
	score = score + 5000;
	updateScore();
});


//Pop Bumpers
// Left Bottom
board2Switches.addListener("switch13RisingEdgeMessage", function() {
	board3Solenoids.setStatus(7, 1, 10);
	score = score + 15;
	updateScore();
});

// Left Top
board2Switches.addListener("switch9RisingEdgeMessage", function() {
	board3Solenoids.setStatus(0, 1, 10);
	score = score + 15;
	updateScore();
});

// Right Top
board2Switches.addListener("switch8RisingEdgeMessage", function() {
	board2Solenoids.setStatus(1, 1, 10);
	score = score + 15;
	updateScore();
});

// Right Bottom
board2Switches.addListener("switch2RisingEdgeMessage", function() {
	board2Solenoids.setStatus(2, 1, 10);
	score = score + 15;
	updateScore();
});

// Drop Targets

var rightDTSwitches = [new dt.Switch(board2Switches, 14), new dt.Switch(board2Switches, 10), new dt.Switch(board2Switches, 6)];
rightDTSwitches[0].addCB(function () { score = score + 20; });
rightDTSwitches[1].addCB(function () { score = score + 20; });
rightDTSwitches[2].addCB(function () { score = score + 20; });


// WICO rollovers
var wicoRollover = 0x00;
// WICO W
board3Switches.addListener("switch13RisingEdgeMessage", function() {
	score = score + 10;
	if(wicoRollover & 0x01) {
		wicoRollover &= 0xFE;
		board3Lamps.setStatus(11, 0);
	} else {
		wicoRollover |= 0x01;
		board3Lamps.setStatus(11, 1);
	}
	if(wicoRollover == 0x0F) {
		completeWICORollover();
	}
	updateScore();
});

// WICO I
board3Switches.addListener("switch9RisingEdgeMessage", function() {
	score = score + 10;
	if(wicoRollover & 0x02) {
		wicoRollover &= 0xFD;
		board2Lamps.setStatus(14, 0);
	} else {
		wicoRollover |= 0x02;
		board2Lamps.setStatus(14, 1);
	}
	if(wicoRollover == 0x0F) {
		completeWICORollover();
	}
	updateScore();
});

// WICO C
board3Switches.addListener("switch1RisingEdgeMessage", function() {
	score = score + 10;
	if(wicoRollover & 0x04) {
		wicoRollover &= 0xFB;
		board2Lamps.setStatus(13, 0);
	} else {
		wicoRollover |= 0x04;
		board2Lamps.setStatus(13, 1);
	}
	if(wicoRollover == 0x0F) {
		completeWICORollover();
	}
	updateScore();
});

// WICO O
board3Switches.addListener("switch5RisingEdgeMessage", function() {
	score = score + 10;
	if(wicoRollover & 0x08) {
		wicoRollover &= 0xF7;
		board2Lamps.setStatus(12, 0);
	} else {
		wicoRollover |= 0x08;
		board2Lamps.setStatus(12, 1);
	}
	if(wicoRollover == 0x0F) {
		completeWICORollover();
	}
	updateScore();
});

function completeWICORollover() {
	board2Lamps.setStatus(12, 1, 255, 20, 0, 20);
	board2Lamps.setStatus(13, 1, 255, 20, 0, 20);
	board2Lamps.setStatus(14, 1, 255, 20, 0, 20);
	board3Lamps.setStatus(11, 1, 255, 20, 0, 20);
	setTimeout(function() {
		wicoRollover = 0x00;
		board2Lamps.setStatus(12, 0);
		board2Lamps.setStatus(13, 0);
		board2Lamps.setStatus(14, 0);
		board3Lamps.setStatus(11, 0);
	}, 5000);
	score = score + 10000;
}

// Score targets near "temple"

// Top left
board3Switches.addListener("switch10RisingEdgeMessage", function() {
	score = score + 10;
	updateScore();
});

// Top right
board2Switches.addListener("switch4RisingEdgeMessage", function() {
	score = score + 10;
	updateScore();
});

// Bottom left
board3Switches.addListener("switch14RisingEdgeMessage", function() {
	score = score + 10;
	updateScore();
});

// Bottom right
board2Switches.addListener("switch0RisingEdgeMessage", function() {
	score = score + 10;
	updateScore();
});

// Outlanes
var laneL1State = 0;
// Far left
board1Switches.addListener("switch7RisingEdgeMessage", function() {
	if(laneL1State) {
		laneL1State = 0;
	} else {
		laneL1State = 1;
	}

	board1Lamps.setStatus(15, laneL1State);
	score = score + 10;
	updateScore();
});

var laneL2State = 0;
// Zone Z
board1Switches.addListener("switch3RisingEdgeMessage", function() {
	if(laneL2State) {
		laneL2State = 0;
	} else {
		laneL2State = 1;
	}

	board1Lamps.setStatus(14, laneL2State);
	score = score + 10;
	updateScore();
});

var laneL3State = 0;
// Zone O
board1Switches.addListener("switch14RisingEdgeMessage", function() {
	if(laneL3State) {
		laneL3State = 0;
	} else {
		laneL3State = 1;
	}

	board1Lamps.setStatus(13, laneL3State);
	score = score + 10;
	updateScore();
});


var laneR1State = 0;
// Zone N
board1Switches.addListener("switch9RisingEdgeMessage", function() {
	if(laneR1State) {
		laneR1State = 0;
	} else {
		laneR1State = 1;
	}

	board2Lamps.setStatus(1, laneR1State);
	score = score + 10;
	updateScore();
});

var laneR2State = 0;
// Zone E
board1Switches.addListener("switch13RisingEdgeMessage", function() {
	if(laneR2State) {
		laneR2State = 0;
	} else {
		laneR2State = 1;
	}

	board2Lamps.setStatus(2, laneR2State);
	score = score + 10;
	updateScore();
});

var laneR3State = 0;
// Far Right
board1Switches.addListener("switch2RisingEdgeMessage", function() {
	if(laneR3State) {
		laneR3State = 0;
	} else {
		laneR3State = 1;
	}

	board2Lamps.setStatus(3, laneR3State);
	score = score + 10;
	updateScore();
});

var multiplier = 1;

function increaseMultiplier() {
	multiplier++;
	if(multiplier > 5) multiplier = 5;
	resetMultiplierLights();
	switch(multiplier) {
		case 5:
			board3Lamps.setStatus(0, 1);
		case 4:
			board3Lamps.setStatus(1, 1);
		case 3:
			board3Lamps.setStatus(2, 1);
		case 2:
			board3Lamps.setStatus(3, 1);
		case 1:
		default:
			break;
	}
}

function resetMultiplierLights() {
	board3Lamps.setStatus(3, 0);
	board3Lamps.setStatus(2, 0);
	board3Lamps.setStatus(1, 0);
	board3Lamps.setStatus(0, 0);
}

var bonus = 0;

function increaseBonus() {
	bonus++;
	if(bonus > 11) bonus = 11;
	resetBonusLights();
	setTimeout(function() {
	switch(bonus) {
		case 11:
			board1Lamps.setStatus(9, 1);
		case 10:
			board1Lamps.setStatus(8, 1);
		case 9:
			board1Lamps.setStatus(10, 1);
		case 8:
			board1Lamps.setStatus(11, 1);
		case 7:
			board1Lamps.setStatus(4, 1);
		case 6:
			board1Lamps.setStatus(5, 1);
		case 5:
			board1Lamps.setStatus(6, 1);
		case 4:
			board1Lamps.setStatus(7, 1);
		case 3:
			board1Lamps.setStatus(0, 1);
		case 2:
			board1Lamps.setStatus(1, 1);
		case 1:
			board1Lamps.setStatus(2, 1);
		default:
			break;
	}}, 200);
}

function resetBonusLights() {
	board1Lamps.setStatus(2, 0);
	board1Lamps.setStatus(1, 0);
	board1Lamps.setStatus(0, 0);
	board1Lamps.setStatus(7, 0);
	board1Lamps.setStatus(6, 0);
	board1Lamps.setStatus(5, 0);
	board1Lamps.setStatus(4, 0);
	board1Lamps.setStatus(11, 0);
	board1Lamps.setStatus(10, 0);
	board1Lamps.setStatus(8, 0);
	board1Lamps.setStatus(9, 0);
}


// Ball drain
board1Switches.addListener("switch15RisingEdgeMessage", function() {
	
	//The ball has drained call functions.
	ballDrain();	


});

//This only works if one pinball is in the machine.
board1Switches.addListener("switch11RisingEdgeMessage", function() {
	
	if(pinball_lifes>0)
	{
		setTimeout(function() {
		board1Solenoids.setStatus(5,1,10);
		}, 1000);
	} else {
		//mpdClient.sendCommand('stop');
	}
	
});



var resetInhibit = 0;
// Start Switch
board3Switches.addListener("switch4RisingEdgeMessage", function() {
	fullReset();

});

function attractMode(){
//make it rain$$$$
}


function ballDrain(){
	//This is for all the stuff that has to happen when a ball drains down the midle of the playfield
	

	//impiortant variables
	pinball_lifes--;

	leftDropTargets.reset();
	setTimeout(function() {
		rightDropTargets.reset();
	}, 200);

	updateBalls();
	console.log("Lives remaining: " + pinball_lifes);




	//solinoids
	setTimeout(function() {
				//reset the left outlane drop targe
				board1Solenoids.setStatus(2, 1, 10);
					
					setTimeout(function() {
						board1Solenoids.setStatus(4, 1, 10);
						//add more solinoids here... board2Solenoids.setStatus(3, 1, 10);
					}, 1000);
	
				
				}, 50);
	}	

function fullReset(){
//This is a full reset of the pinball machine, e.g. start of a new game.
if(resetInhibit == 0) {


		if(pinball_lifes>0)
		{
			board1Solenoids.setStatus(5, 1, 10);
		}else{
			console.log("Full Reset of pinball Machine!");

			// Left bank targets
			leftTargetBank = 0x00;
			board3Lamps.setStatus(4, 0);
			board3Lamps.setStatus(5, 0);
			board3Lamps.setStatus(6, 0);
			board3Lamps.setStatus(7, 0);

			// Right bank targets
			board2Lamps.setStatus(0, 0);
			board2Lamps.setStatus(7, 0);
			board2Lamps.setStatus(6, 0);
			board2Lamps.setStatus(5, 0);
			rightTargetBank = 0;

			// WICO rollovers
			wicoRollover = 0x00;
			board2Lamps.setStatus(12, 0);
			board2Lamps.setStatus(13, 0);
			board2Lamps.setStatus(14, 0);
			board3Lamps.setStatus(11, 0);



			// Outlanes
			laneL1State = 0;
			laneL2State = 0;
			laneL3State = 0;
			laneR1State = 0;
			laneR2State = 0;
			laneR3State = 0;
			board1Lamps.setStatus(15, laneL1State);
			board1Lamps.setStatus(14, laneL1State);
			board1Lamps.setStatus(13, laneL1State);
			board2Lamps.setStatus(1, laneR1State);
			board2Lamps.setStatus(2, laneR1State);
			board2Lamps.setStatus(3, laneR1State);

			bonus = 0;
			resetBonusLights();
			multiplier = 1;
			resetMultiplierLights();

			//Solinoids
			setTimeout(function() {
				//reset the left outlane drop targe
				board1Solenoids.setStatus(2, 1, 10);

				setTimeout(function() {
					//reset the oil pit target
					board3Solenoids.setStatus(1, 1, 10);
					
					//kick the ball load solinoid
					setTimeout(function() {
						board1Solenoids.setStatus(5, 1, 10);
						
						//Reset the Left and Right Drop Targets
						setTimeout(function() {
							leftDropTargets.reset();
							
							setTimeout(function() {
								rightDropTargets.reset();

							//add more solinoids here... board2Solenoids.setStatus(3, 1, 10);
								},50);
							},50);
						},50);
					}, 50);
				}, 50);

			resetInhibit = 1;
			setTimeout(function() { resetInhibit = 0; }, 5000);
			pinball_lifes=2;
			updateBalls();
			score = 0;
			updateScore();
			//mpdClient.sendCommand("play 0");
		}
	}

}

function updateScore() {
	// Update player 1 score to show "Score"
	player1Score.setDisplay(0, Buffer([0x5B, 0x4E, 0x7E, 0x05, 0x4F, 0x00, 0x00, 0x00]));
	player2Score.setScore(0, score);
}

function updateBalls() {
	// Update Player 3 score to show "Balls"
	player3Score.setDisplay(0, Buffer([0x1F, 0x77, 0x0E, 0x0E, 0x5B, 0x00, 0x00, 0x00]));
	player4Score.setScore(0, pinball_lifes);
}

//function playSoundtrack() {
//	player.play(__dirname + "/wavs/soundtrack.wav", function() {
//		console.log("Restart soundtrack");
//		playSoundtrack();
//	});
//}

channel.start();
updateScore();
updateBalls();
//playSoundtrack();
