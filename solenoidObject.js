var events = require('events');
var util = require('util');

var solenoidObject = function(boardNumber, messageDecoder) {
    this.boardNumber = boardNumber;
    this.messageDecoder = messageDecoder;

    var thisInstance = this;

    this.handleCanMessage = function(priority, boardSpecific, boardAddress, featureType, featureNum, messageType, isRequest, data) {
        if(boardAddress == boardNumber) {
            thisInstance.emit('unfilteredSolenoidMessage', priority, boardSpecific, boardAddress, featureType, featureNum, messageType, isRequest, data);
            switch(messageType) {
                case 0:
                    thisInstance.emit('solenoidStatusMessage', featureNum, isRequest, data);
                    break;
                case 1:
                    thisInstance.emit('solenoidDefaultsMessage', featureNum, isRequest, data);
                    break;
                case 2:
                    thisInstance.emit('solenoidTriggeringMessage', featureNum, isRequest, data);
                    break;
                default:
                    break;
            }
        }
    };

    this.requestStatus = function(solenoidNumber, priority, boardSpecific) {
        if(arguments.length < 3) boardSpecific = 1;
        if(arguments.length < 2) priority = 0;
        var dataOut = new Buffer([]);
        messageDecoder.sendCanFrame(priority, boardSpecific, boardNumber, 3, solenoidNumber, 0, true, dataOut);
    };

    this.requestDefaults = function(solenoidNumber, priority, boardSpecific) {
        if(arguments.length < 3) boardSpecific = 1;
        if(arguments.length < 2) priority = 0;
        var dataOut = new Buffer([]);
        messageDecoder.sendCanFrame(priority, boardSpecific, boardNumber, 3, solenoidNumber, 1, true, dataOut);
    };

    this.requestTriggering = function(solenoidNumber, priority, boardSpecific) {
        if(arguments.length < 3) boardSpecific = 1;
        if(arguments.length < 2) priority = 0;
        var dataOut = new Buffer([]);
        messageDecoder.sendCanFrame(priority, boardSpecific, boardNumber, 3, solenoidNumber, 2, true, dataOut);
    };

    this.setStatus = function(solenoidNumber, solenoidStatus, solenoidOnTime, priority, boardSpecific) {
        if(arguments.length < 5) boardSpecific = 1;
        if(arguments.length < 4) priority = 0;
        if(arguments.length >= 3) {
            var dataOut = new Buffer([solenoidStatus, solenoidOnTime]);
        } else if(arguments.length == 2) {
            var dataOut = new Buffer([solenoidStatus]);
        } else {
            return;
        }

        messageDecoder.sendCanFrame(priority, boardSpecific, boardNumber, 3, solenoidNumber, 0, false, dataOut);
    }

    this.setDefaults = function(solenoidNumber, solenoidOnTime, priority, boardSpecific) {
        if(arguments.length < 4) boardSpecific = 1;
        if(arguments.length < 3) priority = 0;
        var dataOut = new Buffer([solenoidOnTime]);

        messageDecoder.sendCanFrame(priority, boardSpecific, boardNumber, 3, solenoidNumber, 1, false, dataOut);
    }

    this.setTriggering = function(solenoidNumber, changeFlag, priority, boardSpecific) {
        if(arguments.length < 4) boardSpecific = 1;
        if(arguments.length < 3) priority = 0;
        if(changeFlag) {
            var triggerByte = 1;
        } else {
            var triggerByte = 0;
        }
        var dataOut = new Buffer([triggerByte]);
        messageDecoder.sendCanFrame(priority, boardSpecific, boardNumber, 3, solenoidNumber, 2, false, dataOut);
    };
}
util.inherits(solenoidObject, events.EventEmitter);
exports.createSolenoidObject = function(boardNum, messageDecoder) { return new solenoidObject(boardNum, messageDecoder); };