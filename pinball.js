var pinballMessageDecoder = require('./pinballMessageDecoder');

var can = require('can');
var channel = can.createRawChannel("can0", true);

pinballMessageDecoder.setCanChannel(channel);
channel.addListener("onMessage", pinballMessageDecoder.incomingCanFrame);

pinballMessageDecoder.addListener("unfilteredMessage", function(priority, boardSpecific, boardAddress, featureType, featureNum, messageType, isRequest, data) {
    if(isRequest) {
        console.log("Request frame received: Priority " + priority + ", address " + boardAddress + ", feature type " + featureType + ", feature number " + featureNum + ", message type " + messageType + ", data " + data.toString('hex'));
    } else {
        console.log("Data frame received: Priority " + priority + ", address " + boardAddress + ", feature type " + featureType + ", feature number " + featureNum + ", message type " + messageType + ", data " + data.toString('hex'));
    }
});

pinballMessageDecoder.addListener("switchMatrixMessage", function(priority, boardSpecific, boardAddress, featureType, featureNum, messageType, isRequest, data) {
    if(messageType == 0) {
        dataOut = new Buffer( [ data[0] ] );
        pinballMessageDecoder.sendCanFrame(priority, 1, boardAddress, 2, featureNum, 0, false, dataOut);
    }
});

channel.start();
