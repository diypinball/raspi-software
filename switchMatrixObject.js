var events = require('events');
var util = require('util');

var switchMatrixObject = function(boardNumber, messageDecoder) {
    this.boardNumber = boardNumber;
    this.messageDecoder = messageDecoder;

    var thisInstance = this;

    this.handleCanMessage = function(priority, boardSpecific, boardAddress, featureType, featureNum, messageType, isRequest, data) {
        if(boardAddress == boardNumber) {
            thisInstance.emit('unfilteredSwitchMatrixMessage', priority, boardSpecific, boardAddress, featureType, featureNum, messageType, isRequest, data);
            switch(messageType) {
                case 0:
                    thisInstance.emit('switchStatusMessage', featureNum, isRequest, data);
                    if(data[1] == 2) {
                        thisInstance.emit('switch' + featureNum + 'FallingEdgeMessage', featureNum, isRequest, data);
                    } else if(data[1] == 1) {
                        thisInstance.emit('switch' + featureNum + 'RisingEdgeMessage', featureNum, isRequest, data);
                    }
                    break;
                case 1:
                    thisInstance.emit('switchPollingMessage', featureNum, isRequest, data);
                    break;
                case 2:
                    thisInstance.emit('switchTriggeringMessage', featureNum, isRequest, data);
                    break;
                default:
                    break;
            }
        }
    };

    this.requestStatus = function(switchNumber, priority, boardSpecific) {
        if(arguments.length < 3) boardSpecific = 1;
        if(arguments.length < 2) priority = 0;
        var dataOut = new Buffer([]);
        messageDecoder.sendCanFrame(priority, boardSpecific, boardNumber, 1, switchNumber, 0, true, dataOut);
    };

    this.requestPolling = function(switchNumber, priority, boardSpecific) {
        if(arguments.length < 3) boardSpecific = 1;
        if(arguments.length < 2) priority = 0;
        var dataOut = new Buffer([]);
        messageDecoder.sendCanFrame(priority, boardSpecific, boardNumber, 1, switchNumber, 1, true, dataOut);
    };

    this.requestTriggering = function(switchNumber, priority, boardSpecific) {
        if(arguments.length < 3) boardSpecific = 1;
        if(arguments.length < 2) priority = 0;
        var dataOut = new Buffer([]);
        messageDecoder.sendCanFrame(priority, boardSpecific, boardNumber, 1, switchNumber, 2, true, dataOut);
    };

    this.setPolling = function(switchNumber, interval, priority, boardSpecific) {
        if(arguments.length < 4) boardSpecific = 1;
        if(arguments.length < 3) priority = 0;
        var dataOut = new Buffer([interval]);
        messageDecoder.sendCanFrame(priority, boardSpecific, boardNumber, 1, switchNumber, 1, false, dataOut);
    };

    this.setTriggering = function(switchNumber, risingEdge, fallingEdge, priority, boardSpecific) {
        if(arguments.length < 5) boardSpecific = 1;
        if(arguments.length < 4) priority = 0;
        var triggerByte = 0;
        if(risingEdge) triggerByte += 2;
        if(fallingEdge) triggerByte += 1;
        var dataOut = new Buffer([triggerByte]);
        messageDecoder.sendCanFrame(priority, boardSpecific, boardNumber, 1, switchNumber, 2, false, dataOut);
    };
}
util.inherits(switchMatrixObject, events.EventEmitter);
exports.createSwitchMatrixObject = function(boardNum, messageDecoder) { return new switchMatrixObject(boardNum, messageDecoder); };
