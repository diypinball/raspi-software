var events = require('events');
var util = require('util');

var systemManagementObject = function(boardNumber, messageDecoder) {
    this.boardNumber = boardNumber;
    this.messageDecoder = messageDecoder;

    var thisInstance = this;

    this.handleCanMessage = function(priority, boardSpecific, boardAddress, featureType, featureNum, messageType, isRequest, data) {
        if(boardAddress == boardNumber) {
            thisInstance.emit('unfilteredSystemManagementMessage', priority, boardSpecific, boardAddress, featureType, featureNum, messageType, isRequest, data);
            switch(messageType) {
                case 0:
                    thisInstance.emit('capabilitiesMessage', isRequest, data);
                    break;
                case 1:
                    thisInstance.emit('versionMessage', isRequest, data);
                    break;
                case 2:
                    thisInstance.emit('powerStatusMessage', isRequest, data);
                    break;
                case 3:
                    thisInstance.emit('powerStatusPollingMessage', isRequest, data);
                    break;
                default:
                    break;
            }
        }
    };

    this.requestCapabilities = function(priority, boardSpecific) {
        if(arguments.length == 1) boardSpecific = 1;
        if(arguments.length == 0) priority = 0;
        var dataOut = new Buffer([]);
        messageDecoder.sendCanFrame(priority, boardSpecific, boardNumber, 0, 0, 0, true, dataOut);
    };

    this.requestVersion = function(priority, boardSpecific) {
        if(arguments.length == 1) boardSpecific = 1;
        if(arguments.length == 0) priority = 0;
        var dataOut = new Buffer([]);
        messageDecoder.sendCanFrame(priority, boardSpecific, boardNumber, 0, 0, 1, true, dataOut);
    };

    this.requestPower = function(priority, boardSpecific) {
        if(arguments.length == 1) boardSpecific = 1;
        if(arguments.length == 0) priority = 0;
        var dataOut = new Buffer([]);
        messageDecoder.sendCanFrame(priority, boardSpecific, boardNumber, 0, 0, 2, true, dataOut);
    };

    this.requestPowerStatusPolling = function(priority, boardSpecific) {
        if(arguments.length == 1) boardSpecific = 1;
        if(arguments.length == 0) priority = 0;
        var dataOut = new Buffer([]);
        messageDecoder.sendCanFrame(priority, boardSpecific, boardNumber, 0, 0, 3, true, dataOut);
    };

    this.setPowerStatusPolling = function(interval, priority, boardSpecific) {
        if(arguments.length < 3) boardSpecific = 1;
        if(arguments.length < 2) priority = 0;
        var dataOut = new Buffer([interval]);
        messageDecoder.sendCanFrame(priority, boardSpecific, boardNumber, 0, 0, 3, false, dataOut);
    };
}
util.inherits(systemManagementObject, events.EventEmitter);
exports.createSystemManagementObject = function(boardNum, messageDecoder) { return new systemManagementObject(boardNum, messageDecoder); };