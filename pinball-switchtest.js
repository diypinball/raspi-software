var pinballMessageDecoder = require('./pinballMessageDecoder');
var switchObjectFactory = require('./switchMatrixObject');
var lampObjectFactory = require('./lampMatrixObject');
var solenoidObjectFactory = require('./solenoidObject');
var systemManagementObjectFactory = require('./systemManagementObject');

var can = require('can');
var channel = can.createRawChannel("can0", true);

pinballMessageDecoder.setCanChannel(channel);
channel.addListener("onMessage", pinballMessageDecoder.incomingCanFrame);

pinballMessageDecoder.addListener("unfilteredMessage", function(priority, boardSpecific, boardAddress, featureType, featureNum, messageType, isRequest, data) {
    if(isRequest) {
        console.log("Request frame received: Priority " + priority + ", address " + boardAddress + ", feature type " + featureType + ", feature number " + featureNum + ", message type " + messageType + ", data " + data.toString('hex'));
    } else {
        console.log("Data frame received: Priority " + priority + ", address " + boardAddress + ", feature type " + featureType + ", feature number " + featureNum + ", message type " + messageType + ", data " + data.toString('hex'));
    }
});

var board1SystemManagement = systemManagementObjectFactory.createSystemManagementObject(1, pinballMessageDecoder);
var board1Switches = switchObjectFactory.createSwitchMatrixObject(1, pinballMessageDecoder);
var board1Lamps = lampObjectFactory.createLampMatrixObject(1, pinballMessageDecoder);
var board1Solenoids = solenoidObjectFactory.createSolenoidObject(1, pinballMessageDecoder);

var board2SystemManagement = systemManagementObjectFactory.createSystemManagementObject(2, pinballMessageDecoder);
var board2Switches = switchObjectFactory.createSwitchMatrixObject(2, pinballMessageDecoder);
var board2Lamps = lampObjectFactory.createLampMatrixObject(2, pinballMessageDecoder);
var board2Solenoids = solenoidObjectFactory.createSolenoidObject(2, pinballMessageDecoder);

var board3SystemManagement = systemManagementObjectFactory.createSystemManagementObject(3, pinballMessageDecoder);
var board3Switches = switchObjectFactory.createSwitchMatrixObject(3, pinballMessageDecoder);
var board3Lamps = lampObjectFactory.createLampMatrixObject(3, pinballMessageDecoder);
var board3Solenoids = solenoidObjectFactory.createSolenoidObject(3, pinballMessageDecoder);

pinballMessageDecoder.addListener("systemManagementMessage", board1SystemManagement.handleCanMessage);
pinballMessageDecoder.addListener("switchMatrixMessage", board1Switches.handleCanMessage);
pinballMessageDecoder.addListener("lampMatrixMessage", board1Lamps.handleCanMessage);
pinballMessageDecoder.addListener("solenoidMessage", board1Solenoids.handleCanMessage);

pinballMessageDecoder.addListener("systemManagementMessage", board2SystemManagement.handleCanMessage);
pinballMessageDecoder.addListener("switchMatrixMessage", board2Switches.handleCanMessage);
pinballMessageDecoder.addListener("lampMatrixMessage", board2Lamps.handleCanMessage);
pinballMessageDecoder.addListener("solenoidMessage", board2Solenoids.handleCanMessage);

pinballMessageDecoder.addListener("systemManagementMessage", board3SystemManagement.handleCanMessage);
pinballMessageDecoder.addListener("switchMatrixMessage", board3Switches.handleCanMessage);
pinballMessageDecoder.addListener("lampMatrixMessage", board3Lamps.handleCanMessage);
pinballMessageDecoder.addListener("solenoidMessage", board3Solenoids.handleCanMessage);

//board1SystemManagement.addListener("capabilitiesMessage", function() {
//	console.log("Setup board 1");
//	board1Lamps.setDefaults(0, 255, 64, 0, 64);
//	board1Solenoids.setDefaults(1, 100);
//});

board3Switches.addListener("switch12RisingEdgeMessage", function() {
	console.log("Rise12");
	board3Lamps.setStatus(0, 1);
	board3Lamps.setStatus(1, 1);
	board3Lamps.setStatus(2, 1);
	board3Lamps.setStatus(3, 1);
	board3Lamps.setStatus(4, 1);
	board3Lamps.setStatus(5, 1);
	board3Lamps.setStatus(6, 1);
	board3Lamps.setStatus(7, 1);
	board3Lamps.setStatus(8, 1);
	board3Lamps.setStatus(9, 1);
	board3Lamps.setStatus(10, 1);
	board3Lamps.setStatus(11, 1);
	board3Lamps.setStatus(12, 1);
	board3Lamps.setStatus(13, 1);
	board3Lamps.setStatus(14, 1);
	board3Lamps.setStatus(15, 1);
});

board3Switches.addListener("switch12FallingEdgeMessage", function() {
	console.log("Fall12");
	board3Lamps.setStatus(0, 0);
	board3Lamps.setStatus(1, 0);
	board3Lamps.setStatus(2, 0);
	board3Lamps.setStatus(3, 0);
	board3Lamps.setStatus(4, 0);
	board3Lamps.setStatus(5, 0);
	board3Lamps.setStatus(6, 0);
	board3Lamps.setStatus(7, 0);
	board3Lamps.setStatus(8, 0);
	board3Lamps.setStatus(9, 0);
	board3Lamps.setStatus(10, 0);
	board3Lamps.setStatus(11, 0);
	board3Lamps.setStatus(12, 0);
	board3Lamps.setStatus(13, 0);
	board3Lamps.setStatus(14, 0);
	board3Lamps.setStatus(15, 0);
});

board3Switches.addListener("switch8RisingEdgeMessage", function() {
	console.log("Rise");
	board2Lamps.setStatus(0, 1);
	board2Lamps.setStatus(1, 1);
	board2Lamps.setStatus(2, 1);
	board2Lamps.setStatus(3, 1);
	board2Lamps.setStatus(4, 1);
	board2Lamps.setStatus(5, 1);
	board2Lamps.setStatus(6, 1);
	board2Lamps.setStatus(7, 1);
	board2Lamps.setStatus(8, 1);
	board2Lamps.setStatus(9, 1);
	board2Lamps.setStatus(10, 1);
	board2Lamps.setStatus(11, 1);
	board2Lamps.setStatus(12, 1);
	board2Lamps.setStatus(13, 1);
	board2Lamps.setStatus(14, 1);
	board2Lamps.setStatus(15, 1);
});

board3Switches.addListener("switch8FallingEdgeMessage", function() {
	console.log("Fall");
	board2Lamps.setStatus(0, 0);
	board2Lamps.setStatus(1, 0);
	board2Lamps.setStatus(2, 0);
	board2Lamps.setStatus(3, 0);
	board2Lamps.setStatus(4, 0);
	board2Lamps.setStatus(5, 0);
	board2Lamps.setStatus(6, 0);
	board2Lamps.setStatus(7, 0);
	board2Lamps.setStatus(8, 0);
	board2Lamps.setStatus(9, 0);
	board2Lamps.setStatus(10, 0);
	board2Lamps.setStatus(11, 0);
	board2Lamps.setStatus(12, 0);
	board2Lamps.setStatus(13, 0);
	board2Lamps.setStatus(14, 0);
	board2Lamps.setStatus(15, 0);
});

//board1Switches.addListener("switch1RisingEdgeMessage", function() {
//	board1Solenoids.setStatus(0, 1, 100);
//});

//board1Switches.addListener("switch1FallingEdgeMessage", function() {
//	board1Solenoids.setStatus(1, 1);
//});

channel.start();
//board1SystemManagement.requestCapabilities(0, 0);
