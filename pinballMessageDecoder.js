var events = require('events');
var util = require('util');

var pinballMessageDecoder = function() {
    var canChannel;

    var thisInstance = this;

    this.setCanChannel = function(channel) {
        canChannel = channel;
    }

    this.decodeArbField = function(addressToDecode) {
        var priority = (addressToDecode & 0x1E000000) >>> 25;
        var boardSpecific = (addressToDecode & 0x01000000) >>> 24;
        var boardAddress = (addressToDecode & 0x00FF0000) >>> 16;
        var featureType = (addressToDecode & 0x0000F000) >>> 12;
        var featureNum = (addressToDecode & 0x00000F00) >> 8;
        var messageType = (addressToDecode & 0x000000F0) >> 4;
        return [priority, boardSpecific, boardAddress, featureType, featureNum, messageType]; 
    };

    this.encodeArbField = function(priority, boardSpecific, boardAddress, featureType, featureNum, messageType) {
        var arbField = 0;
        arbField |= (priority & 0x0000000F) << 25;
        arbField |= (boardSpecific & 0x00000001) << 24;
        arbField |= (boardAddress & 0x000000FF) << 16;
        arbField |= (featureType & 0x0000000F) << 12;
        arbField |= (featureNum & 0x0000000F) << 8;
        arbField |= (messageType & 0x0000000F) << 4;

        return arbField;
    }

    this.incomingCanFrame = function (canFrame) {
        var priority = 0, boardSpecific = 0, boardAddress = 0, featureType = 0, featureNum = 0, messageType = 0, isRequest = 0;
        var returnArr = thisInstance.decodeArbField(canFrame.id);
        priority = returnArr[0];
        boardSpecific = returnArr[1];
        boardAddress = returnArr[2];
        featureType = returnArr[3];
        featureNum = returnArr[4];
        messageType = returnArr[5];
        isRequest = canFrame.rtr;
        thisInstance.emit('unfilteredMessage', priority, boardSpecific, boardAddress, featureType, featureNum, messageType, isRequest, canFrame.data);
        switch(featureType) {
            case 0:
                thisInstance.emit('systemManagementMessage', priority, boardSpecific, boardAddress, featureType, featureNum, messageType, isRequest, canFrame.data);
                break;
            case 1:
                thisInstance.emit('switchMatrixMessage', priority, boardSpecific, boardAddress, featureType, featureNum, messageType, isRequest, canFrame.data);
                break;
            case 2:
                thisInstance.emit('lampMatrixMessage', priority, boardSpecific, boardAddress, featureType, featureNum, messageType, isRequest, canFrame.data);
                break;
            case 3:
                thisInstance.emit('solenoidMessage', priority, boardSpecific, boardAddress, featureType, featureNum, messageType, isRequest, canFrame.data);
                break;
            case 4:
                thisInstance.emit('scoreDisplayMessage', priority, boardSpecific, boardAddress, featureType, featureNum, messageType, isRequest, canFrame.data);
                break;
            default:
                thisInstance.emit('unknownMessage', priority, boardSpecific, boardAddress, featureType, featureNum, messageType, isRequest, canFrame.data);
                break;
        }
    };

    this.sendCanFrame = function(priority, boardSpecific, boardAddress, featureType, featureNum, messageType, isRequest, data) {
        var arbField = thisInstance.encodeArbField(priority, boardSpecific, boardAddress, featureType, featureNum, messageType);

        var newFrame = new Object();
        newFrame.data = data;
        newFrame.id = arbField;
        newFrame.ext = true;
        if(isRequest) newFrame.rtr = true;

        if(canChannel) {
            return canChannel.send(newFrame);
        } else {
            return false;
        }
    };
}
util.inherits(pinballMessageDecoder, events.EventEmitter);
module.exports = new pinballMessageDecoder();
