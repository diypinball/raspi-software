function Light(board, number) {
	this.board = board;
	this.number = number;

	var self = this;

	this.set = function(state) {
		self.board.setStatus(self.number, state);
	};

	this.blink = function(onTime, offTime) {
		onTime = (onTime || 500) / 10;
		offTime = (offTime || 500) / 10;
		self.board.setStatus(self.number, 1, 255, onTime, 0, offTime);
	};
};

function Solenoid(board, number) {
	this.board = board;
	this.number = number;

	this.set = function(state) {
		this.board.setStatus(this.number, state, 10);
	}

	this.setPerm = function(state) {
		this.board.setStatus(this.number, state);
	}
};

function Switch(board, number) {
	this.board = board;
	this.number = number;
	this.cb = [];
	this.fallingCB = [];

	var self = this;

	this._eventCB = function() {
		for (var i = 0; i < self.cb.length; i++) {
			self.cb[i]();
		}
	};

	this._fallingEventCB = function() {
		for (var i = 0; i < self.fallingCB.length; i++) {
			self.fallingCB[i]();
		}
	};

	this.board.addListener("switch" + number + "RisingEdgeMessage", self._eventCB);
	this.board.addListener("switch" + number + "FallingEdgeMessage", self._fallingEventCB);

	this.addCB = function(cb) {
		self.cb.push(cb);
	};

	this.addFallingCB = function(cb) {
		self.fallingCB.push(cb);
	};
};

function DropTargets(switches, lights, solenoids, targetSolenoid, completeCB) {
	this.dropTargets = 0;
	this.switches = switches;
	this.lights = lights;
	this.solenoids = solenoids;
	this.targetSolenoid = targetSolenoid;
	this.completeCB = completeCB;
	this.timeout = undefined;
	this.timeoutTime = 10000;
	this.ignoreInput = false;

	var self = this;

	this.hitSwitch = function(number) {
		if (self.ignoreInput) {
			return;
		}

		if (self.dropTargets == 0) {
			self.blinkLights();

			// start reset timer
			if (self.timeoutTime > 0) {
				self.timeout = setTimeout(self.reset, self.timeoutTime);
			}
		}

		self.dropTargets |= (1 << number);
		if (self.dropTargets == ((1 << self.switches.length) - 1))
		{
			self.reset();
			if (self.targetSolenoid !== null) {
				self.targetSolenoid.set(1, 20);
			}
			self.completeCB();
		}
	};

	this.blinkLights = function() {
		var delay = 0;
		for (var i = 0; i < this.lights.length; i++) {
			self._blinkLight(i, delay);
			delay += 250;
		}
	};

	this.reset = function() {
		if (self.timeout) {
			clearTimeout(self.timeout);
			self.timeout = undefined;
		}

		// ignore spurious input while targets are being reset
		setTimeout(function() { self.ignoreInput = true; }, 10);
		setTimeout(function() { self.ignoreInput = false; }, 250);

		if (self.dropTargets != 0x00)
		{
			self.dropTargets = 0x00;
		}

		for (var i = 0; i < self.lights.length; i++) {
			self.lights[i].set(0);
		}

		var timeout = 0;
		for (i = 0; i < self.solenoids.length; i++) {
			self._clearSolenoid(i, timeout);
			timeout += 50;
		}
	};

	this.enabled = function () {
		return !self.ignoreInput;
	};

	this.setTimeoutTime = function(time) {
		self.timeoutTime = time;
	};

	this._blinkLight = function(number, delay) {
		delay = delay || 0;
		if (delay == 0) {
			self.lights[number].blink(500, 500);
		} else {
			setTimeout(function() {
				self.lights[number].blink(500, 500);
			}, delay);
		}
	};

	this._clearSolenoid = function(number, delay) {
		if (delay == undefined || delay == 0) {
			self.solenoids[number].set(1, 10);
		} else {
			setTimeout(function() {
				self.solenoids[number].set(1, 10);
			}, delay);
		}
	};

	this._setSwitchCallback = function(swNumber) {
		self.switches[swNumber].addCB(function() {
			self.hitSwitch(swNumber);
		});
	};

	for (var i = 0; i < self.switches.length; i++)
	{
		self._setSwitchCallback(i);
	}
};

exports.Light = Light;
exports.Switch = Switch;
exports.Solenoid = Solenoid;
exports.DropTargets = DropTargets;
