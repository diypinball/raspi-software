var pinballMessageDecoder = require('./pinballMessageDecoder');
var switchObjectFactory = require('./switchMatrixObject');
var lampObjectFactory = require('./lampMatrixObject');
var solenoidObjectFactory = require('./solenoidObject');
var systemManagementObjectFactory = require('./systemManagementObject');
var scoreModuleObjectFactory = require('./scoreModuleObject');
var dt = require('./drop-targets');
var mt = require('./multi-target');

var can = require('socketcan');
var channel = can.createRawChannel("can0", true);
var pinball_lives=0;

var score = 0;
var high_score = 0;

pinballMessageDecoder.setCanChannel(channel);
channel.addListener("onMessage", pinballMessageDecoder.incomingCanFrame);

pinballMessageDecoder.addListener("unfilteredMessage", function(priority, boardSpecific, boardAddress, featureType, featureNum, messageType, isRequest, data) {
    if(isRequest) {
        console.log("Request frame received: Priority " + priority + ", address " + boardAddress + ", feature type " + featureType + ", feature number " + featureNum + ", message type " + messageType + ", data " + data.toString('hex'));
    } else {
        console.log("Data frame received: Priority " + priority + ", address " + boardAddress + ", feature type " + featureType + ", feature number " + featureNum + ", message type " + messageType + ", data " + data.toString('hex'));
    }
});

var board1SystemManagement = systemManagementObjectFactory.createSystemManagementObject(1, pinballMessageDecoder);
var board1Switches = switchObjectFactory.createSwitchMatrixObject(1, pinballMessageDecoder);
var board1Lamps = lampObjectFactory.createLampMatrixObject(1, pinballMessageDecoder);
var board1Solenoids = solenoidObjectFactory.createSolenoidObject(1, pinballMessageDecoder);

var board2SystemManagement = systemManagementObjectFactory.createSystemManagementObject(2, pinballMessageDecoder);
var board2Switches = switchObjectFactory.createSwitchMatrixObject(2, pinballMessageDecoder);
var board2Lamps = lampObjectFactory.createLampMatrixObject(2, pinballMessageDecoder);
var board2Solenoids = solenoidObjectFactory.createSolenoidObject(2, pinballMessageDecoder);

var board3SystemManagement = systemManagementObjectFactory.createSystemManagementObject(3, pinballMessageDecoder);
var board3Switches = switchObjectFactory.createSwitchMatrixObject(3, pinballMessageDecoder);
var board3Lamps = lampObjectFactory.createLampMatrixObject(3, pinballMessageDecoder);
var board3Solenoids = solenoidObjectFactory.createSolenoidObject(3, pinballMessageDecoder);

var lifeBoard = scoreModuleObjectFactory.createScoreModuleObject(4, pinballMessageDecoder);
var player1Score = scoreModuleObjectFactory.createScoreModuleObject(6, pinballMessageDecoder);
var player2Score = scoreModuleObjectFactory.createScoreModuleObject(5, pinballMessageDecoder);
var player3Score = scoreModuleObjectFactory.createScoreModuleObject(7, pinballMessageDecoder);

pinballMessageDecoder.addListener("systemManagementMessage", board1SystemManagement.handleCanMessage);
pinballMessageDecoder.addListener("switchMatrixMessage", board1Switches.handleCanMessage);
pinballMessageDecoder.addListener("lampMatrixMessage", board1Lamps.handleCanMessage);
pinballMessageDecoder.addListener("solenoidMessage", board1Solenoids.handleCanMessage);

pinballMessageDecoder.addListener("systemManagementMessage", board2SystemManagement.handleCanMessage);
pinballMessageDecoder.addListener("switchMatrixMessage", board2Switches.handleCanMessage);
pinballMessageDecoder.addListener("lampMatrixMessage", board2Lamps.handleCanMessage);
pinballMessageDecoder.addListener("solenoidMessage", board2Solenoids.handleCanMessage);

pinballMessageDecoder.addListener("systemManagementMessage", board3SystemManagement.handleCanMessage);
pinballMessageDecoder.addListener("switchMatrixMessage", board3Switches.handleCanMessage);
pinballMessageDecoder.addListener("lampMatrixMessage", board3Lamps.handleCanMessage);
pinballMessageDecoder.addListener("solenoidMessage", board3Solenoids.handleCanMessage);


// create IO objects
var leftFlipper = new dt.Solenoid(board1Solenoids, 1);
var rightFlipper = new dt.Solenoid(board1Solenoids, 3);
leftButton = new dt.Switch(board3Switches, 1);
rightButton = new dt.Switch(board3Switches, 0);
leftButton.addCB(function () {
    leftFlipper.setPerm(1);
});
leftButton.addFallingCB(function () {
    leftFlipper.setPerm(0);
});
rightButton.addCB(function () {
    rightFlipper.setPerm(1);
});
rightButton.addFallingCB(function () {
    rightFlipper.setPerm(0);
});

// static lights
board1Lamps.setStatus(3, 1);
board1Lamps.setStatus(4, 1);
board1Lamps.setStatus(5, 1);
board1Lamps.setStatus(6, 1);
board1Lamps.setStatus(11, 1);
board1Lamps.setStatus(15, 1);
board2Lamps.setStatus(2, 1);
board2Lamps.setStatus(3, 1);
board2Lamps.setStatus(4, 1);
board2Lamps.setStatus(6, 1);
board2Lamps.setStatus(15, 1);

// left target
var leftTarget = new dt.Switch(board1Switches, 12);
leftTarget.addCB(function() {
    score += 500;
    updateScore();
});

// ABCDEF targets
var targetSwitches = [new dt.Switch(board2Switches, 2), new dt.Switch(board1Switches, 1), new dt.Switch(board2Switches, 3),
                      new dt.Switch(board2Switches, 9), new dt.Switch(board1Switches, 6), new dt.Switch(board2Switches, 10)];


function setCBonAll(list, cb) {
    for (var i = 0; i < list.length; i++) {
        list[i].addCB(cb);
    }
}

setCBonAll(targetSwitches, function() {
    score += 500;
    updateScore();
});

var targets = new mt.MultiTarget(
        targetSwitches,
        [new dt.Light(board2Lamps, 7), new dt.Light(board1Lamps, 13), new dt.Light(board2Lamps, 5),
         new dt.Light(board2Lamps, 0), new dt.Light(board1Lamps, 2), new dt.Light(board2Lamps, 12)],
        function () {
            score += 20000;
            updateScore();
        });

var specialTargets = new mt.MultiTarget(
        [new dt.Switch(board1Switches, 2), new dt.Switch(board1Switches, 7)],
        [new dt.Light(board1Lamps, 14), new dt.Light(board1Lamps, 3)],
        function () {
            score += 5000;
        });

// drop targets
var rightDTSwitches = [new dt.Switch(board1Switches, 8),
    new dt.Switch(board1Switches, 9),
    new dt.Switch(board1Switches, 10),
    new dt.Switch(board1Switches, 11)];

var dropTargets = new dt.DropTargets(rightDTSwitches,
                                     [],
                                     [new dt.Solenoid(board2Solenoids, 5)],
                                     null,
                                     function () {
                                         score += 5000;
                                         updateScore();
                                     });

setCBonAll(rightDTSwitches, function() {
    score += 500;
    updateScore();
});

dropTargets.setTimeoutTime(0);

// spin target
var spinTarget = new dt.Switch(board2Switches, 12);
spinTarget.addCB(function() {
    score += 250;
    updateScore();
});

// ball kick
var drainSensor = new dt.Switch(board1Switches, 3);
var startButton = new dt.Switch(board3Switches, 6);
var ballKicker = new dt.Solenoid(board1Solenoids, 2);

var startDisabled = false;
var kicked = false;

drainSensor.addCB(function () {
    if (pinball_lives > 0 && !kicked)
    {
        kicked = true;
        setTimeout(function () {
            pinball_lives -= 1;
            updateBalls();
            ballKicker.set(1);
            kicked = false;
        }, 200);
    }
    else
    {
        // game over
        updateFinalScore();
    }
    startDisabled = false;
});

function startGame() {
    if (startDisabled) {
        return;
    } else if (pinball_lives > 0) {
    } else {
        // new game
        score = 0;
        pinball_lives = 2;
        updateScore();
	setTimeout(updateBalls, 50);
	setTimeout(function() {
            targets.reset();
        }, 100);
        setTimeout(function() {
            specialTargets.reset();
        }, 150);
        setTimeout(function() {
            dropTargets.reset();
        }, 200);
        setTimeout(function() {
            player3Score.setDisplay(0, createBytes("playball"));
        }, 250);
    }

    ballKicker.set(1);
    startDisabled = true;
    setTimeout(function () {
            startDisabled = false;
        }, 5000);
}

startButton.addCB(startGame);

// pop bumpers
var leftPopSensor = new dt.Switch(board2Switches, 6);
var leftPopBumper = new dt.Solenoid(board2Solenoids, 2);
leftPopSensor.addCB(function () {
    leftPopBumper.set(1);
    score += 10;
    updateScore();
});

var centerPopSensor = new dt.Switch(board2Switches, 7);
var centerPopBumper = new dt.Solenoid(board2Solenoids, 3);
centerPopSensor.addCB(function () {
    centerPopBumper.set(1);
    score += 10;
    updateScore();
});

var rightPopSensor = new dt.Switch(board2Switches, 8);
var rightPopBumper = new dt.Solenoid(board2Solenoids, 4);
rightPopSensor.addCB(function () {
    rightPopBumper.set(1);
    score += 10;
    updateScore();
});

var leftKickSensor = new dt.Switch(board1Switches, 0);
var leftKicker = new dt.Solenoid(board1Solenoids, 0);
leftKickSensor.addCB(function () {
    leftKicker.set(1);
    score += 5;
    updateScore();
});

var rightKickSensor = new dt.Switch(board1Switches, 4);
var rightKicker = new dt.Solenoid(board1Solenoids, 4);
rightKickSensor.addCB(function () {
    rightKicker.set(1);
    score += 5;
    updateScore();
});

function incrementScore(pts, count, deltaT) {
    if (count > 0) {
        score += pts;
        updateScore();
        setTimeout(function () {
            incrementScore(pts, count - 1, deltaT);
        }, deltaT);
    }
}

// traps
var centerTrapSensor = new dt.Switch(board2Switches, 1);
var centerTrapKicker = new dt.Solenoid(board2Solenoids, 0);
var centerTrapEnabled = true;
centerTrapSensor.addCB(function () {
    if (centerTrapEnabled) {
        centerTrapEnabled = false;
        incrementScore(20, 50, 30);
        setTimeout(function () {
            centerTrapKicker.set(1);
            centerTrapEnabled = true;
        }, 1500);
    }
});


var leftTrapSensor = new dt.Switch(board2Switches, 4);
var leftTrapKicker = new dt.Solenoid(board2Solenoids, 1);
var leftTrapEnabled = true;
leftTrapSensor.addCB(function () {
    if (leftTrapEnabled) {
        leftTrapEnabled = false;
        incrementScore(20, 50, 30);
        setTimeout(function () {
            leftTrapKicker.set(1);
            leftTrapEnabled = true;
        }, 1500);
    }
});

// kick out ball on boot
setTimeout(function() {
    centerTrapKicker.set(1);
    leftTrapKicker.set(1);
}, 25);


var charLUT = {};
charLUT[' '] = 0x00;
charLUT['a'] = 0x77;
charLUT['b'] = 0x1F;
charLUT['c'] = 0x4E;
charLUT['e'] = 0x4F;
charLUT['g'] = 0x7B;
charLUT['i'] = 0x06;
charLUT['l'] = 0x0E;
charLUT['o'] = 0x7E;
charLUT['p'] = 0x67;
charLUT['r'] = 0x05;
charLUT['s'] = 0x5B;
charLUT['u'] = 0x1C;
charLUT['v'] = 0x1C;
charLUT['y'] = 0x3B;
charLUT['.'] = 0x80;


function createBytes(str) {
    str = str.substr(0, 8);
    str = str.toLowerCase();

    var bytes = [];
    for (var i = 0; i < str.length; ++i) {
        bytes.push(charLUT[str.charAt(i)]);
    }

    for (var j = 0; j < 8 - bytes.length; ++j) {
        bytes.push(0x00);
    }

    return Buffer(bytes);
}

function updateScore() {
    // Update player 1 score to show "Score"
    if (score > high_score) {
        high_score = score;
    }

    player1Score.setScore(0, score);
    player2Score.setScore(0, high_score);
}

function updateBalls() {
    // Update Player 3 score to show "Balls"
    lifeBoard.setScore(0, pinball_lives);
}

function updateFinalScore() {
    player3Score.setDisplay(0, createBytes("playover"));
}

channel.start();
updateScore();
updateBalls();

setTimeout(startGame, 1500);

//playSoundtrack();
