var testCase = require('nodeunit').testCase;

var dt = require('../drop-targets');

var MockBoard = function() {
	this._reset = function() {
		this.outputs = [];
		for (var i = 0; i < 10; i++) {
			this.outputs.push({state: 0, onTime: undefined});
		}

		this.prio = undefined;
		this.boardSpecific = undefined;
	};

	this.setStatus = function(number, state, onTime, prio, boardSpecific) {
		this.outputs[number].state = state;
		this.outputs[number].onTime = onTime;

		this.prio = prio;
		this.boardSpecific = boardSpecific;
	};

	this.addListener = function(name, cb) {
	};

	this._reset();
};

var board = new MockBoard();

module.exports = testCase({
	setUp: function (callback) {
		board._reset();
		callback();
	},

	"IO Classes": testCase({
		"Light": function(test) {
			var l = new dt.Light(board, 2);
			l.set(1);

			test.equals(board.outputs[2].state, 1);
			test.equals(board.outputs[2].onTime, undefined);

			test.done();
		},

		"Solenoid": function(test) {
			var s = new dt.Solenoid(board, 3, 10);
			s.set(0);

			test.equals(board.outputs[3].state, 0);
			test.equals(board.outputs[3].onTime, 10);

			test.done();
		},

		"Switch": function(test) {
			var s = new dt.Switch(board, 4);
			var result = 0;

			s.addCB(function () {
				result = 1;
			});

			s._eventCB();

			test.equals(result, 1);
			test.done();
		}
	}),

	"DropTarget": function(test) {
		var switches = [ new dt.Switch(board, 0), new dt.Switch(board, 1) ];
		var lights = [ new dt.Light(board, 8), new dt.Light(board, 9) ];
		var sols = [ new dt.Solenoid(board, 4), new dt.Solenoid(board, 5) ];
		var blocker = new dt.Solenoid(board, 3);

		var cbCalled = 0;

		var dropTargets = new dt.DropTargets(switches, lights, sols, blocker, function() {
			cbCalled = 1;
		});

		switches[0]._eventCB();
		// check that lights are on
		test.equals(board.outputs[8].state, 1);

		switches[1]._eventCB();

		// check that bumper has been raised
		test.equals(board.outputs[3].state, 1);

		// check that callback was called
		test.equals(cbCalled, 1);

		// check that lights are reset
		test.equals(board.outputs[8].state, 0);
		test.equals(board.outputs[9].state, 0);

		setTimeout(function () {
			// check that solenoids are activated
			test.equals(board.outputs[4].state, 1);
			test.equals(board.outputs[4].onTime, 10);
			test.equals(board.outputs[5].state, 1);
			test.equals(board.outputs[5].onTime, 10);

			test.done();
		}, 100);
	}
});
