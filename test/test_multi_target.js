var testCase = require('nodeunit').testCase;

var dt = require('../drop-targets');
var mt = require('../multi-target');

var MockBoard = function() {
	this._reset = function() {
		this.outputs = [];
		for (var i = 0; i < 10; i++) {
			this.outputs.push({state: 0, onTime: undefined});
		}

		this.prio = undefined;
		this.boardSpecific = undefined;
	};

	this.setStatus = function(number, state, onTime, prio, boardSpecific) {
		this.outputs[number].state = state;
		this.outputs[number].onTime = onTime;

		this.prio = prio;
		this.boardSpecific = boardSpecific;
	};

	this.addListener = function(name, cb) {
	};

	this._reset();
};

var board = new MockBoard();

module.exports = testCase({
	setUp: function (callback) {
		board._reset();
		callback();
	},

	"MultiTarget": function(test) {
		var switches = [ new dt.Switch(board, 0), new dt.Switch(board, 1), new dt.Switch(board, 2) ];
		var lights = [ new dt.Light(board, 7), new dt.Light(board, 8), new dt.Light(board, 9) ];

		var cbCalled = 0;

		var multiTarget = new mt.MultiTarget(switches, lights, function() {
			cbCalled = 1;
		});

		switches[0]._eventCB();
		// check that light 1 is on
		test.equals(board.outputs[7].state, 1);

		switches[1]._eventCB();
		// check that light 2 is on
		test.equals(board.outputs[8].state, 1);

		switches[2]._eventCB();

		// check that callback was called
		test.equals(cbCalled, 1);

		// check that lights are reset
		test.equals(board.outputs[8].state, 0);
		test.equals(board.outputs[9].state, 0);

		test.done();
	}
});
