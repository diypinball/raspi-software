var pinballMessageDecoder = require('./pinballMessageDecoder');
var switchObjectFactory = require('./switchMatrixObject');
var lampObjectFactory = require('./lampMatrixObject');
var solenoidObjectFactory = require('./solenoidObject');
var systemManagementObjectFactory = require('./systemManagementObject');

var can = require('can');
var channel = can.createRawChannel("can0", true);
var pinball_lifes=0;



pinballMessageDecoder.setCanChannel(channel);
channel.addListener("onMessage", pinballMessageDecoder.incomingCanFrame);

pinballMessageDecoder.addListener("unfilteredMessage", function(priority, boardSpecific, boardAddress, featureType, featureNum, messageType, isRequest, data) {
    if(isRequest) {
        console.log("Request frame received: Priority " + priority + ", address " + boardAddress + ", feature type " + featureType + ", feature number " + featureNum + ", message type " + messageType + ", data " + data.toString('hex'));
    } else {
        console.log("Data frame received: Priority " + priority + ", address " + boardAddress + ", feature type " + featureType + ", feature number " + featureNum + ", message type " + messageType + ", data " + data.toString('hex'));
    }
});

var board1SystemManagement = systemManagementObjectFactory.createSystemManagementObject(1, pinballMessageDecoder);
var board1Switches = switchObjectFactory.createSwitchMatrixObject(1, pinballMessageDecoder);
var board1Lamps = lampObjectFactory.createLampMatrixObject(1, pinballMessageDecoder);
var board1Solenoids = solenoidObjectFactory.createSolenoidObject(1, pinballMessageDecoder);

var board2SystemManagement = systemManagementObjectFactory.createSystemManagementObject(2, pinballMessageDecoder);
var board2Switches = switchObjectFactory.createSwitchMatrixObject(2, pinballMessageDecoder);
var board2Lamps = lampObjectFactory.createLampMatrixObject(2, pinballMessageDecoder);
var board2Solenoids = solenoidObjectFactory.createSolenoidObject(2, pinballMessageDecoder);

var board3SystemManagement = systemManagementObjectFactory.createSystemManagementObject(3, pinballMessageDecoder);
var board3Switches = switchObjectFactory.createSwitchMatrixObject(3, pinballMessageDecoder);
var board3Lamps = lampObjectFactory.createLampMatrixObject(3, pinballMessageDecoder);
var board3Solenoids = solenoidObjectFactory.createSolenoidObject(3, pinballMessageDecoder);

pinballMessageDecoder.addListener("systemManagementMessage", board1SystemManagement.handleCanMessage);
pinballMessageDecoder.addListener("switchMatrixMessage", board1Switches.handleCanMessage);
pinballMessageDecoder.addListener("lampMatrixMessage", board1Lamps.handleCanMessage);
pinballMessageDecoder.addListener("solenoidMessage", board1Solenoids.handleCanMessage);

pinballMessageDecoder.addListener("systemManagementMessage", board2SystemManagement.handleCanMessage);
pinballMessageDecoder.addListener("switchMatrixMessage", board2Switches.handleCanMessage);
pinballMessageDecoder.addListener("lampMatrixMessage", board2Lamps.handleCanMessage);
pinballMessageDecoder.addListener("solenoidMessage", board2Solenoids.handleCanMessage);

pinballMessageDecoder.addListener("systemManagementMessage", board3SystemManagement.handleCanMessage);
pinballMessageDecoder.addListener("switchMatrixMessage", board3Switches.handleCanMessage);
pinballMessageDecoder.addListener("lampMatrixMessage", board3Lamps.handleCanMessage);
pinballMessageDecoder.addListener("solenoidMessage", board3Solenoids.handleCanMessage);

//Flippers
board3Switches.addListener("switch12RisingEdgeMessage", function() {
	board1Solenoids.setStatus(6, 1);
});

board3Switches.addListener("switch12FallingEdgeMessage", function() {
	board1Solenoids.setStatus(6, 0);
});

board3Switches.addListener("switch8RisingEdgeMessage", function() {
	board1Solenoids.setStatus(7, 1);
});

board3Switches.addListener("switch8FallingEdgeMessage", function() {
	board1Solenoids.setStatus(7, 0);
});
//End Flippers



board1Switches.addListener("switch10RisingEdgeMessage", function() {
	board1Solenoids.setStatus(0, 1, 10);
});

board1Switches.addListener("switch6RisingEdgeMessage", function() {
	board1Solenoids.setStatus(1, 1, 10);
});

board3Switches.addListener("switch2RisingEdgeMessage", function() {
	setTimeout(function() {board3Solenoids.setStatus(2, 1, 10);}, 2000);
});


//Pop Bumpers
board2Switches.addListener("switch13RisingEdgeMessage", function() {
	board3Solenoids.setStatus(7, 1, 10);
});

board2Switches.addListener("switch9RisingEdgeMessage", function() {
	board3Solenoids.setStatus(0, 1, 10);
});

board2Switches.addListener("switch8RisingEdgeMessage", function() {
	board2Solenoids.setStatus(1, 1, 10);
});

board2Switches.addListener("switch2RisingEdgeMessage", function() {
	board2Solenoids.setStatus(2, 1, 10);
});



board1Switches.addListener("switch8RisingEdgeMessage", function() {
	resetLeftDropTargets();
});

board1Switches.addListener("switch12RisingEdgeMessage", function() {
	resetRightDropTargets();
});

function resetLeftDropTargets() {
	console.log("Reset left drop targets");
	//Lamps
	board3Lamps.setStatus(10,1);
	board3Lamps.setStatus(11,1);
	board3Lamps.setStatus(4,1);

	board3Solenoids.setStatus(4, 1, 10);
	setTimeout(function() {
		board3Solenoids.setStatus(5, 1, 10);
		setTimeout(function() {
			board3Solenoids.setStatus(6, 1, 10);
		}, 50);
	}, 50);
}

function resetRightDropTargets() {
	console.log("Reset right drop targets");
	//Lamps
	board3Lamps.setStatus(8,1);
	board3Lamps.setStatus(9,1);
	board3Lamps.setStatus(15,1);

	//Solinoids
	board2Solenoids.setStatus(5, 1, 10);
	setTimeout(function() {
		board2Solenoids.setStatus(4, 1, 10);
		setTimeout(function() {
			board2Solenoids.setStatus(3, 1, 10);
		}, 50);
	}, 50);
}

board1Switches.addListener("switch15RisingEdgeMessage", function() {
	
	//The ball has drained call functions.
	ballDrain();	


});

//This only works if one pinball is in the machine.
board1Switches.addListener("switch11RisingEdgeMessage", function() {
	
	if(pinball_lifes>0)
	{
		setTimeout(function() {
		board1Solenoids.setStatus(5,1,10);
		}, 1000);
	}
	
});


var leftTargetCount = 0x0F;

board2Switches.addListener("switch15RisingEdgeMessage", function() {
	board2Lamps.setStatus(0, 1); 
	leftTargetCount = leftTargetCount & 0x0E;
	console.log("Left Target Count: " + leftTargetCount);
	if(leftTargetCount == 0) {
		leftTargetReset();
	}
});

board2Switches.addListener("switch11RisingEdgeMessage", function() {
	board2Lamps.setStatus(7, 1); 
	leftTargetCount = leftTargetCount & 0x0D;
	console.log("Left Target Count: " + leftTargetCount);
	if(leftTargetCount == 0) {
		leftTargetReset();
	}
});

board2Switches.addListener("switch7RisingEdgeMessage", function() {
	board2Lamps.setStatus(6, 1); 
	leftTargetCount = leftTargetCount & 0x0B;
	console.log("Left Target Count: " + leftTargetCount);
	if(leftTargetCount == 0) {
		leftTargetReset();
	}
});

board2Switches.addListener("switch3RisingEdgeMessage", function() {
	board2Lamps.setStatus(5, 1); 
	leftTargetCount = leftTargetCount & 0x07;
	console.log("Left Target Count: " + leftTargetCount);
	if(leftTargetCount == 0) {
		leftTargetReset();
	}
});

function leftTargetReset() {
	board2Lamps.setStatus(0, 1, 255, 20, 0, 20); 
	board2Lamps.setStatus(7, 1, 255, 20, 0, 20); 
	board2Lamps.setStatus(6, 1, 255, 20, 0, 20); 
	board2Lamps.setStatus(5, 1, 255, 20, 0, 20); 
	setTimeout(function() {
		leftTargetCount = 0x0f;
		board2Lamps.setStatus(0, 0);
		board2Lamps.setStatus(7, 0);
		board2Lamps.setStatus(6, 0);
		board2Lamps.setStatus(5, 0);
	}, 5000);
}

var resetInhibit = 0;

board3Switches.addListener("switch4RisingEdgeMessage", function() {
	fullReset();	

});

var laneL1State = 0;

board1Switches.addListener("switch9RisingEdgeMessage", function() {
	if(laneL1State) {
		laneL1State = 0;
	} else {
		laneL1State = 1;
	}

	board2Lamps.setStatus(1, laneL1State);
});

var laneL2State = 0;

board1Switches.addListener("switch13RisingEdgeMessage", function() {
	if(laneL2State) {
		laneL2State = 0;
	} else {
		laneL2State = 1;
	}

	board2Lamps.setStatus(2, laneL2State);
});

var laneL3State = 0;

board1Switches.addListener("switch2RisingEdgeMessage", function() {
	if(laneL3State) {
		laneL3State = 0;
	} else {
		laneL3State = 1;
	}

	board2Lamps.setStatus(3, laneL3State);
});

var right_droptarget_bank=0x07;

board3Switches.addListener("switch7RisingEdgeMessage", function() {
	board3Lamps.setStatus(8, 0); 
	right_droptarget_bank = right_droptarget_bank & 0x06;
	console.log("Right target bank: " + right_droptarget_bank);
	if(right_droptarget_bank == 0) {
		right_droptarget_bank = 0x07;
		//This is actually the right drop targets but shh dont tell anyone.
		resetLeftDropTargets();
	}
});	

board3Switches.addListener("switch11RisingEdgeMessage", function() {
	board3Lamps.setStatus(9, 0); 
	right_droptarget_bank = right_droptarget_bank & 0x05;
	console.log("Right target bank: " + right_droptarget_bank);
	if(right_droptarget_bank == 0) {
		right_droptarget_bank = 0x07;
		//This is actually the right drop targets but shh dont tell anyone.
		resetLeftDropTargets();
	}
});	

board3Switches.addListener("switch13RisingEdgeMessage", function() {
	board3Lamps.setStatus(15, 0); 
	right_droptarget_bank = right_droptarget_bank & 0x03;
	console.log("Right target bank: " + right_droptarget_bank);
	if(right_droptarget_bank == 0) {
		right_droptarget_bank = 0x07;
		//This is actually the right drop targets but shh dont tell anyone.
		resetLeftDropTargets();
	}
});	

var left_droptarget_bank=0x07;

board2Switches.addListener("switch14RisingEdgeMessage", function() {
	board2Lamps.setStatus(10, 0); 
	left_droptarget_bank = left_droptarget_bank & 0x06;
	console.log("Left target bank: " + left_droptarget_bank);
	if(left_droptarget_bank == 0) {
		left_droptarget_bank = 0x07;
		//This is actually the left drop targets but shh dont tell anyone.
		resetRightDropTargets();
	}
});	

board2Switches.addListener("switch10RisingEdgeMessage", function() {
	board2Lamps.setStatus(11, 0); 
	left_droptarget_bank = left_droptarget_bank & 0x05;
	console.log("Left target bank: " + left_droptarget_bank);
	if(left_droptarget_bank == 0) {
		left_droptarget_bank = 0x07;
		//This is actually the left drop targets but shh dont tell anyone.
		resetRightDropTargets();
	}
});	

board2Switches.addListener("switch6RisingEdgeMessage", function() {
	board2Lamps.setStatus(4, 0); 
	left_droptarget_bank = left_droptarget_bank & 0x03;
	console.log("Left target bank: " + left_droptarget_bank);
	if(left_droptarget_bank == 0) {
		left_droptarget_bank = 0x07;
		//This is actually the left drop targets but shh dont tell anyone.
		resetRightDropTargets();
	}
});	

function attractMode(){
//make it rain$$$$
}


function ballDrain(){
	//This is for all the stuff that has to happen when a ball drains down the midle of the playfield
	

	//impiortant variables
	pinball_lifes--;
	console.log("Lives remaining: " + pinball_lifes);




	//solinoids
	setTimeout(function() {
				//reset the left outlane drop targe
				board1Solenoids.setStatus(2, 1, 10);
					
					setTimeout(function() {
						board1Solenoids.setStatus(4, 1, 10);
						//add more solinoids here... board2Solenoids.setStatus(3, 1, 10);
					}, 1000);
	
				
				}, 50);
		
}


function fullReset(){
//This is a full reset of the pinball machine, e.g. start of a new game.
if(resetInhibit == 0) {


		if(pinball_lifes>0)
		{
			board1Solenoids.setStatus(5, 1, 10);
		}else{
			console.log("Full Reset of pinball Machine!");
			
			board1Solenoids.setStatus(5, 1, 10);
			board2Lamps.setStatus(0, 0);
			board2Lamps.setStatus(7, 0);
			board2Lamps.setStatus(6, 0);
			board2Lamps.setStatus(5, 0);
			leftTargetCount = 0;
			resetLeftDropTargets();
			resetRightDropTargets();
				
			//Solinoids
	
			setTimeout(function() {
				//reset the left outlane drop targe
				board1Solenoids.setStatus(2, 1, 10);
					
				setTimeout(function() {
					//reset the oil pit target
					board3Solenoids.setStatus(1, 1, 10);
					
						//add more solinoids here... board2Solenoids.setStatus(3, 1, 10);
				
					}, 50);
				}, 50);

					
			left_droptarget_bank=0x07;
			right_droptarget_bank=0x07;
			resetInhibit = 1;
			setTimeout(function() { resetInhibit = 0; }, 5000);
			pinball_lifes=4;
		}
	}

}

channel.start();
